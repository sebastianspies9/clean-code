package net.decix.cleancode.csv;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TableFormatter {

	public String formatPage(List<String> columnNames, List<Map<String, String>> pageData, int currentPage,
			int maxPage) {
		StringBuffer buffer = new StringBuffer();

		Map<String, Integer> columnWidths = computeColumnWidth(columnNames, pageData);

		formatHeader(columnNames, columnWidths, buffer);
		formatRows(columnNames, pageData, columnWidths, buffer);
		buffer.append("\n");
		// Kata II.2 - Show which page is displayed out of how many in total below the
		// table.
		formatPageIndex(currentPage, maxPage, buffer);
		// end of Kata II.2
		buffer.append("\n");
		formatHelp(buffer);

		return buffer.toString();
	}

	private void formatPageIndex(int currentPage, int maxPage, StringBuffer buffer) {
		buffer.append("Page " + (currentPage + 1) + " of " + (maxPage + 1));
	}

	private void formatHelp(StringBuffer buffer) {
		buffer.append("N)ext page, P)revious page, F)irst page, L)ast page, eX)it: ");
	}

	public String formatJumpToPagePrompt() {
		return "Please enter page to jump to: ";
	}
	
	public String formatSortByColumnPrompt() {
		return "Please enter column name to sort on: ";
	}

	private void formatHeader(List<String> columnNames, Map<String, Integer> columnWidths, StringBuffer buffer) {
		Map<String, String> headerRow = columnNames.stream()
				.collect(Collectors.toMap(Function.identity(), Function.identity()));
		formatSeparatorLine(columnNames, columnWidths, buffer);

		formatRow(columnNames, headerRow, columnWidths, buffer);

		formatSeparatorLine(columnNames, columnWidths, buffer);
	}

	private void formatRows(List<String> columnNames, List<Map<String, String>> pageData,
			Map<String, Integer> columnWidths, StringBuffer buffer) {
		for (Map<String, String> row : pageData) {
			formatRow(columnNames, row, columnWidths, buffer);
			formatSeparatorLine(columnNames, columnWidths, buffer);
		}
	}

	private void formatRow(List<String> columnNames, Map<String, String> row, Map<String, Integer> columnWidths,
			StringBuffer buffer) {
		buffer.append("|");
		for (String columnName : columnNames) {
			int columnWidth = columnWidths.get(columnName);
			buffer.append(" ");
			String formatString = "%1$-" + Integer.toString(columnWidth) + "s |";
			buffer.append(String.format(formatString, row.get(columnName)));
		}
		buffer.append("\n");
	}

	private static String valueToSeparator(int width) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < width + 2; i++)
			buffer.append("-");

		return buffer.toString();
	}

	private void formatSeparatorLine(List<String> columnNames, Map<String, Integer> columnWidths, StringBuffer buffer) {

		buffer.append("+");
		buffer.append(columnNames.stream().map(columnWidths::get).map(TableFormatter::valueToSeparator)
				.collect(Collectors.joining("+")));
		buffer.append("+\n");
	}

	protected Map<String, Integer> computeColumnWidth(List<String> columnNames, List<Map<String, String>> pageData) {
		return columnNames.stream()
				.map(columnName -> Map.entry(columnName, computeColumnWidthForColumnName(columnName, pageData)))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));
	}

	protected int computeColumnWidthForColumnName(String columnName, List<Map<String, String>> pageData) {
		int dataMaximum = pageData.stream().map(map -> map.get(columnName)).mapToInt(String::length).max().orElse(0);
		return Math.max(columnName.length(), dataMaximum);
	}

	public static TableFormatter standardTable() {
		return new TableFormatter();
	};
}
