package net.decix.cleancode.csv;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import net.decix.cleancode.csv.Table;

public class TestTable {

	@Test
	public void testReadSchemaFromLine() {
		List<String> columnNames = Table.readSchemaFromLine("");
		assertEquals(1, columnNames.size());
		assertEquals("", columnNames.get(0));
		
		columnNames = Table.readSchemaFromLine(";");
		assertEquals(2, columnNames.size());
		assertEquals("", columnNames.get(0));
		assertEquals("", columnNames.get(1));
		
		columnNames = Table.readSchemaFromLine(";a;");
		assertEquals(3, columnNames.size());
		assertEquals("a", columnNames.get(1));
		
		
	}
}
	//
